package main

import (
	"log"
	"proxy_pool/api"
	"proxy_pool/entity"
	"proxy_pool/gain"
	"proxy_pool/storage"
	"sync"
	"time"
)

func main() {

	// 执行api服务
	go func() {
		api.Run()
	}()

	ipChan := make(chan *entity.IPEntity, 2000)

	for i := 0; i < 50; i++ {
		go func() {
			for {
				storage.CheckIP(<-ipChan)
			}
		}()
	}

	for {
		n := len(storage.CheckedIps)
		log.Printf("Chan: %v, IP: %v\n", len(ipChan), n)
		if len(ipChan) < 100 {
			go run(ipChan)
		}
		time.Sleep(10 * time.Minute)
	}
}

// 调用查询代理
func run(ipChan chan<- *entity.IPEntity) {
	var wg sync.WaitGroup
	funs := []func() []*entity.IPEntity{
		gain.Kuaidl,
		gain.Ip89,
		gain.ListPlus,
		gain.Kxdl,
	}

	for _, f := range funs {
		wg.Add(1)
		go func(f func() []*entity.IPEntity) {
			temp := f()
			//log.Println("[run] get into loop")
			for _, v := range temp {
				// log.Println("[run] len of ipChan %v", v)
				ipChan <- v
			}
			wg.Done()
		}(f)
	}
}
