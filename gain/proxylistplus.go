package gain

import (
	"log"
	"proxy_pool/entity"
	"strconv"
	"time"

	"github.com/PuerkitoBio/goquery"
	"github.com/parnurzeal/gorequest"
)

func ListPlus() (result []*entity.IPEntity) {
	for i := 1; i < 6; i++ {
		pUrl := "https://list.proxylistplus.com/Fresh-HTTP-Proxy-List-" + strconv.Itoa(i)
		time.Sleep(10 * time.Second)
		data := dealListPlusIp(pUrl)
		if len(data) > 0 {
			for i := 0; i < len(data); i++ {
				result = append(result, data[i])
			}
		}
	}
	return
}

func dealListPlusIp(pollURL string) (result []*entity.IPEntity) {
	resp, _, errs := gorequest.New().Get(pollURL).End()
	if errs != nil {
		log.Println("listplus请求错误信息：", errs)
		return
	}
	if resp.StatusCode != 200 {
		log.Println("listplus请求失败信息：", errs)
		return
	}
	// fmt.Println(resp.Body)
	doc, err := goquery.NewDocumentFromReader(resp.Body)
	resp.Body.Close()
	if err != nil {
		log.Println("listplus请求页面失败信息：", err.Error())
		return
	}
	doc.Find("table[cellspacing='1']>tbody>tr").Each(func(i int, s *goquery.Selection) {
		if i > 1 {
			ip := s.Find("td:nth-child(2)").Text()
			port := s.Find("td:nth-child(3)").Text()
			// log.Println(ip, port)
			InitIP := entity.InitIPEntity()
			InitIP.Ip = ip
			InitIP.Port = port
			InitIP.IpType = "HTTP"
			result = append(result, InitIP)
		}
	})
	log.Println(pollURL + "   finish!!! ")
	return
}
