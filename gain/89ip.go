package gain

import (
	"log"
	"proxy_pool/entity"
	"strings"

	"github.com/PuerkitoBio/goquery"
	"github.com/parnurzeal/gorequest"
)

func Ip89() (result []*entity.IPEntity) {
	pollURL := "https://www.89ip.cn/tqdl.html?num=100&address=&kill_address=&port=&kill_port=&isp="
	resp, _, errs := gorequest.New().Get(pollURL).End()
	if errs != nil {
		log.Println("89ip请求错误信息：", errs)
		return
	}
	if resp.StatusCode != 200 {
		log.Println("89ip请求失败信息：", errs)
		return
	}
	// fmt.Println(resp.Body)
	doc, err := goquery.NewDocumentFromReader(resp.Body)
	resp.Body.Close()
	if err != nil {
		log.Println("89ip请求页面失败信息：", err.Error())
		return
	}
	doc.Find(".layui-col-md8 > .fly-panel > div").Each(func(i int, s *goquery.Selection) {
		content, err := s.Html()
		if err != nil {
			log.Println(err.Error())
		}
		if content != "" {
			kv := strings.Split(content, "<br/>")
			for i := range kv {
				// 最后有文字输入  排除掉
				if i != len(kv)-1 {
					va := kv[i]
					vas := strings.Split(va, ":")
					InitIP := entity.InitIPEntity()
					InitIP.Ip = strings.Trim(vas[0], "")
					InitIP.Port = strings.Trim(vas[1], "")
					InitIP.IpType = "HTTP"
					result = append(result, InitIP)
				}
			}
		}
	})
	log.Println(pollURL + "   finish!!! ")
	return
}
