package api

import (
	"encoding/json"
	"log"
	"net/http"
	"proxy_pool/conf"
	"proxy_pool/storage"
)

const VERSION = "/v1"

func Run() {
	mux := http.NewServeMux()
	mux.HandleFunc(VERSION+"/ip", ProxyHandler)
	log.Println("Starting server", conf.HttpAddr+":"+conf.HttpPort)
	http.ListenAndServe(conf.HttpAddr+":"+conf.HttpPort, mux)
}

// ProxyHandler .
func ProxyHandler(w http.ResponseWriter, r *http.Request) {
	if r.Method == "GET" {
		w.Header().Set("content-type", "application/json")
		b, err := json.Marshal(storage.ProxyRandom())
		if err != nil {
			return
		}
		w.Write(b)
	}
}
