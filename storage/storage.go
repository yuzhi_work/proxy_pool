package storage

import (
	"log"
	"math/rand"
	"proxy_pool/entity"
)

var CheckedIps []*entity.IPEntity

func ProxyRandom() (ip *entity.IPEntity) {
	if CheckedIps == nil {
		log.Println("CheckedIps 为null")
		return entity.InitIPEntity()
	}
	var l = len(CheckedIps)
	if l <= 0 {
		log.Println("CheckedIps 数量为空")
		return entity.InitIPEntity()
	}
	randomNum := rand.Intn(l)
	var ipe = CheckedIps[randomNum]

	return ipe
}
