module proxy_pool

go 1.16

require (
	github.com/PuerkitoBio/goquery v1.7.1
	github.com/elazarl/goproxy v0.0.0-20210110162100-a92cc753f88e // indirect
	github.com/parnurzeal/gorequest v0.2.16
	github.com/pkg/errors v0.9.1 // indirect
	github.com/smartystreets/goconvey v1.6.4 // indirect
	moul.io/http2curl v1.0.0 // indirect
)
