package entity

import "fmt"

type IPEntity struct {
	Ip       string
	Port     string
	IpType   string
	Location string
	Speed    string
	LastTime string
}

func InitIPEntity() *IPEntity {
	return &IPEntity{Speed: "100"}
}

func (t *IPEntity) String() string {
	return fmt.Sprintf("{Ip:%v, Port:%v, IpType:%v, Location:%v, Speed:%v, LastTime:%v}", t.Ip, t.Port, t.IpType, t.Location, t.Speed, t.LastTime)
}
